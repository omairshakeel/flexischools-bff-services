﻿Write-Host "Going to deploy to AWS"
Write-Host "Current Location"
Get-Location

# package and upload to s3 
$guid = [guid]::newguid()
$stackName = "${bamboo.shortPlanName}"
$stackName = $stackName.replace(".", "-")

$packageName = "$stackName/${guid}-package.zip"

Write-Host "Stack name: " $stackName
Write-Host "Package name: " $packageName

Write-Host "Uploading Package  "  $packageName " to S3 bucket: " $Env:BAMBOO_AWS_DeploymentPackages_S3
aws s3api put-object --bucket $Env:BAMBOO_AWS_DeploymentPackages_S3 --key $packageName --body Flexischools.Backend.UserServices.A.Api.zip --profile bamboo
Write-Host "Upload Completed"

$CodeUri = """CodeUri"": ""s3://$Env:BAMBOO_AWS_DeploymentPackages_S3/$packageName"""
Write-Host "Updating CodeUri in template to " $CodeUri

# Update the CodeUri field in serverless.template file and save the file in this folder
((Get-Content -path ./Deploy/serverless.template -Raw) -replace '"CodeUri": ""', $CodeUri) | Set-Content -Path serverless.template
Write-Host "Packaging completed!"

Write-Host "Deploying ServerLess Application to QA on stack " $stackName

# Deploy to AWS
Write-Host "Deploying to stack " $stackName
aws cloudformation deploy --template-file serverless.template --stack-name $stackName --capabilities CAPABILITY_IAM --parameter-overrides ShouldCreateBucket=false BucketName=serverless-example --region $Env:BAMBOO_AWSRegion --profile bamboo

