using System;
using System.Collections.Generic;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Flexischools.BFF.Serverless.Api.Events;
using Flexischools.BFF.Serverless.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Flexischools.BFF.Serverless.Api.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IAmazonSimpleNotificationService _snsService;

        public UserController(IAmazonSimpleNotificationService snsService)
        {
            _snsService = snsService;
        }

        // GET api/user
        [HttpGet]
        //[ProducesResponseType(typeof(UserDto), 200)]
        public IEnumerable<string> Get()
        {
            return new string[] { "You have reached the User BFF Api" };
        }

        // GET api/user/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return $"User {id}";
        }

        // POST api/user
        [HttpPost]
        [ProducesResponseType(typeof(UserCreated), 200)]
        public UserCreated Post([FromBody]UserModel value)
        {
            var guid = new Guid();

            // Call the service that creates a user - get UserCreated event
            var userCreated = new UserCreated
            {
                CreatedOn = DateTime.Now.ToString("hh:mm:ss:fff"),
                Guid = guid.ToString(),
                UserId = 5,
                IpAddress = HttpContext.Connection.RemoteIpAddress.ToString()
            };

            // Send user created event to the SNS topic
            // Publish it to SNS async
            var request = new PublishRequest
            {
                Message = JsonConvert.SerializeObject(userCreated),
                TopicArn = "arn:aws:sns:us-east-2:130738311877:flexischools-services-events-usercreated"
            };

            var response = _snsService.PublishAsync(request).Result;

            return userCreated;
        }
    }
}