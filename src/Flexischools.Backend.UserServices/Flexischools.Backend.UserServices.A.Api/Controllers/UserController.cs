using System;
using System.Threading.Tasks;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Flexischools.Backend.UserServices.A.Api.Commands;
using Flexischools.Backend.UserServices.A.Api.Events;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Flexischools.Backend.UserServices.A.Api.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IAmazonSimpleNotificationService _snsService;
        private IMediator _mediator;

        private static bool snsSent = false;

        public UserController(IAmazonSimpleNotificationService snsService, IMediator mediator)
        {
            _snsService = snsService;
            _mediator = mediator;
        }

        [HttpGet("{id}")]
        public async Task<string> Get(int id)
        {
            SnsEventPublisher publisher = new SnsEventPublisher(_snsService);

            Console.WriteLine("Sns sent value: " + snsSent);
            //if (!snsSent)
            //{
            //    publisher.Publish("Hello there - synchronous!", "arn:aws:sns:ap-southeast-2:191596069677:poc-test-sns");
            //    snsSent = true;
            //}
            
            Console.WriteLine("Sending from new thread");
            await publisher.PublishInNewThread("Hello from new thread", "arn:aws:sns:ap-southeast-2:191596069677:poc-test-sns");

            // Syncrhonous
            //await publisher.PrintTopicsList();
            //var arns = publisher.TopicsArns;

            // Asynchronous
            //publisher.PrintTopicsInThread();

            return "";
            //var command = new CreateUserCommand();
            //var commandResult = _mediator.Send(command);

            //var c2 = new CreateUser2Command();
            //var r = _mediator.Send(c2);

            //return commandResult.Result.ToString();

            //var userCreated = new UserCreated
            //{
            //    Guid = "guid-1",
            //    Email = "omair@omair.com",
            //    UserId = 1,
            //    InitiatorIpAddress = "192.168.0.1"
            //};

            //// Publish it to SNS async
            //var request = new PublishRequest
            //{
            //    Message = JsonConvert.SerializeObject(userCreated),
            //    TopicArn = "arn:aws:sns:us-east-2:130738311877:flexischools-services-events-usercreated"
            //};

            //try
            //{
            //    var response = _snsService.PublishAsync(request).Result;
            //}
            //catch (Exception e)
            //{
            //    return "Error: " + e.Message;
            //}

            //return Startup.startupTime.TotalMilliseconds.ToString("#.##");
        }

        // POST api/values
        [HttpPost]
        [ProducesResponseType(typeof(UserCreated), 200)]
        public UserCreated Post([FromBody]CreateUserCommand command)
        {
            // Save the user in database
            var userId = 1;

            // Generate and send the UserCreated event
            var userCreated = new UserCreated
            {
                Guid = command.Guid,
                Email = command.Email,
                UserId = userId,
                InitiatorIpAddress = command.InitiatorIpAddress
            };

            // Publish it to SNS async
            var request = new PublishRequest
            {
                Message = JsonConvert.SerializeObject(userCreated),
                TopicArn = "arn:aws:sns:us-east-2:130738311877:flexischools-services-events-usercreated"
            };

            _snsService.PublishAsync(request);

            return userCreated;
        }
    }
}
