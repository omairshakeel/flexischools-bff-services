﻿namespace Flexischools.Backend.UserServices.A.Api.Events
{
    public class UserCreated
    {
        public string Guid { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
        public string InitiatorIpAddress { get; set; }
    }
}
