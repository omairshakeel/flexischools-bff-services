﻿param(
    [String]$accessKey,
    [String]$secretKey,
    [String]$subnetAName,
    [String]$subnetBName,
    [String]$serverlessSgName
)

if (!$accessKey) {
    throw "Deployment not possible as the AWS AccessKey is not defined"
}

if (!$secretKey) {
    throw "Deployment is not possible as the AWS SecretKey is not defined"
}

if (!$subnetAName) {
    throw "SubnetA name is missing"
}

if (!$subnetBName) {
    throw "SubnetB name is missing"
}

# Get the stack name (int/qa/prod)
$stackName = Get-ChildItem Env:bamboo_deploy_environment | Select-Object -first 1
Write-Host "Deploying to Stack: " $stackName.Value

# Set credentials
Set-AWSCredentials -AccessKey $accessKey -SecretKey $secretKey

# Get the account stack
$account = Get-CFNStack -StackName "account"

Write-Host "Got the CFN Stack"

# Get the subnet and security groups
foreach ($output in $account.Outputs) {
    if ($output.OutputKey -eq $subnetAName) {
        $subNetA = $output
    }
    if ($output.OutputKey -eq $subnetBName) {
        $subNetB = $output
    }
    if ($output.OutputKey -eq $serverlessSgName) {
        $sg = $output
    }
}

if (!$subNetA) {
    throw "$subnetAName not defined in the Account stack's output"
}
Write-Host "SubnetA: " $subNetA.OutputValue

if (!$subNetB) {
    throw "$subnetBName not defined in the Account stack's output"
}
Write-Host "SubnetB: " $subNetB.OutputValue

if (!$sg) {
    throw "$serverlessSgName not defined in the Account stack's output"
}
Write-Host "SG: " $sg.OutputValue