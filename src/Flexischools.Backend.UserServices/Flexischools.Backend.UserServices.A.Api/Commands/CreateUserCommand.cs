﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Flexischools.Backend.UserServices.A.Api.Commands
{
    public class CreateUserCommand : IRequest<bool>
    {
        public string Guid { get; set; }
        public string Email { get; set; }
        public string InitiatorIpAddress { get; set; }
    }

    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, bool>
    {
        public Task<bool> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            return new Task<bool>(() => true);
        }
    }
}
