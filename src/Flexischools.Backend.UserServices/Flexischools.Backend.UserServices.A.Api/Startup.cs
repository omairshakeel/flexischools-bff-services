using System;
using System.Linq;
using System.Reflection;
using Amazon.SimpleNotificationService;
using Flexischools.Backend.UserServices.A.Api.Commands;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Flexischools.Backend.UserServices.A.Api
{
    public class Startup
    {
        public const string AppS3BucketKey = "AppS3Bucket";

        public static TimeSpan startupTime;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            DateTime start = DateTime.Now;

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Add S3 to the ASP.NET Core dependency injection framework.
            services.AddAWSService<Amazon.S3.IAmazonS3>();

            // This expects to be able to pick up valid credentials from one of the default locations
            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
            
            // Register SNS service
            services.AddAWSService<IAmazonSimpleNotificationService>();

            // Register Mediator and IRequestHandlers
            services.AddMediatR();
            
            startupTime = DateTime.Now.Subtract(start);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
