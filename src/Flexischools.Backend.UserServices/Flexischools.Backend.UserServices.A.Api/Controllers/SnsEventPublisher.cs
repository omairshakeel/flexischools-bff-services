﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.S3.Model;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;

namespace Flexischools.Backend.UserServices.A.Api.Controllers
{
    public class SnsEventPublisher
    {
        private readonly IAmazonSimpleNotificationService _snsService;

        public SnsEventPublisher(IAmazonSimpleNotificationService snsService)
        {
            _snsService = snsService;
        }

        public async Task PublishInNewThread(string message, string topicArn)
        {
            //var publishAction = new ThreadStart(() =>
            //{
                
            //});
            //var thread = new Thread(publishAction);
            //thread.Start();

            await Task.Run(async () =>
            {
                Console.WriteLine("Publish Thread started");

                var request = new PublishRequest { Message = message, TopicArn = topicArn };

                try
                {
                    Console.WriteLine("Going to publish message in new thread");

                    var response = await _snsService.PublishAsync(request);

                    Console.WriteLine("Message published: " + response.MessageId);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            });
        }

        public void Publish(string message, string topicArn)
        {
            var request = new PublishRequest { Message = message, TopicArn = topicArn };

            try
            {
                Console.WriteLine("Going to publish message");

                var response = _snsService.PublishAsync(request).Result;

                Console.WriteLine("Message published: " + response.MessageId);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        public string TopicsArns { get; set; }

        public async Task PrintTopicsList()
        {
            Console.WriteLine("Getting topics list now");

            var topics = await _snsService.ListTopicsAsync(new ListTopicsRequest());
            PrintTopics(topics);
        }

        public void PrintTopicsInThread()
        {
            Task.Run(() =>
            {
                Console.WriteLine("Getting topics list in new thread");

                var topics = _snsService.ListTopicsAsync(new ListTopicsRequest()).Result;

                PrintTopics(topics);
            });
        }

        private void PrintTopics(ListTopicsResponse topics)
        {
            Console.WriteLine("Topics count: " + topics.Topics.Count);

            var topicArns = "";
            foreach (var topic in topics.Topics)
            {
                topicArns += topic.TopicArn;
            }

            TopicsArns = topicArns;
            Console.WriteLine(topicArns);
        }
    }
}
