using Amazon.SimpleNotificationService;
using Xunit;
using Flexischools.BFF.Serverless.Api.Controllers;
using Moq;

namespace Flexischools.BFF.Serverless.Api.UnitTests.Controllers
{
    public class UserControllerTests
    {
        [Fact]
        public void GetShouldReturnValue()
        {
            // Arrange
            var controllerToTest = new UserController(Mock.Of<IAmazonSimpleNotificationService>());

            // Assert
            var output = controllerToTest.Get(12);


            // Act
            Assert.True(output== "User 12");
        }
    }
}
