﻿using System;
using System.Collections.Generic;

namespace Flexischools.BFF.Serverless.Api.Events
{
    public class UserCreated
    {
        public string CreatedOn { get; set; }
        public string Guid { get; set; }
        public long UserId { get; set; }
        public string IpAddress { get; set; }
        public List<string> Errors { get; set; }
    }
}
